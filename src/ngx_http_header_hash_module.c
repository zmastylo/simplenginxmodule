#include "ngx_http_header_hash_module.h"

ngx_module_t ngx_http_header_hash_module;
static u_char char_hash[SIZE];

/*
 * Module command
*/
static ngx_command_t ngx_http_header_hash_commands[] = {

    {
      ngx_string("header_hash"),
      NGX_HTTP_LOC_CONF|NGX_CONF_NOARGS,
      ngx_http_header_hash,
      0,
      0,
      NULL
    },

    ngx_null_command
};

/*
 * Module content
 */
static ngx_http_module_t ngx_http_header_hash_module_ctx = {
    NULL, /* preconfiguration */
    NULL, /* postconfiguration */

    NULL, /* create main configuration */
    NULL, /* init main configuration */

    NULL, /* create server configuration */
    NULL, /* merge server configuration */

    NULL, /* create location configuration */
    NULL  /* merge location configuration */
};

/*
 * Module definition
 */
ngx_module_t ngx_http_header_hash_module = {
    NGX_MODULE_V1,
    &ngx_http_header_hash_module_ctx, /* module context */
    ngx_http_header_hash_commands,    /* module directives */
    NGX_HTTP_MODULE,                  /* module type */
    NULL,                             /* init master */
    NULL,                             /* init module */
    NULL,                             /* init process */
    NULL,                             /* init thread */
    NULL,                             /* exit thread */
    NULL,                             /* exit process */
    NULL,                             /* exit master */
    NGX_MODULE_V1_PADDING
};

ngx_int_t ngx_http_header_hash_handler(ngx_http_request_t* r)
{
    ngx_buf_t *b;
    ngx_chain_t out;
    ngx_uint_t  ii, hash;

    /* Set Content-Type header. */
    r->headers_out.content_type.len = sizeof("text/plain") - 1;
    r->headers_out.content_type.data = (u_char *) "text/plain";

    b = ngx_pcalloc(r->pool, sizeof(ngx_buf_t));

    out.buf = b;
    out.next = NULL;

    /* Calculate hash based on request header lowercase naem */
    hash = 0;
    for(ii = 0; ii < NGX_HTTP_LC_HEADER_LEN; ++ii) {
      hash = ngx_hash(hash, r->lowcase_header[ii]);
    }

    sprintf((char*)char_hash, "%ud", (unsigned int)hash);

    b->start = b->pos = char_hash;
    b->end = b->last = b->pos + SIZE; 
    b->memory = 1; 
    b->last_buf = 1; 
    b->last_in_chain = 1;

    r->headers_out.status = NGX_HTTP_OK;
    r->headers_out.content_length_n = sizeof(SIZE);
    ngx_http_send_header(r);

    printf("Hello Zbigniew");
    return ngx_http_output_filter(r, &out);
}

char* ngx_http_header_hash(ngx_conf_t* cf, ngx_command_t* cmd, void* conf)
{
    ngx_http_core_loc_conf_t *clcf; 

    clcf = ngx_http_conf_get_module_loc_conf(cf, ngx_http_core_module);
    clcf->handler = ngx_http_header_hash_handler;

    return NGX_CONF_OK;
}

